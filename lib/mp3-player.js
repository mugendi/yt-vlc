const path = require('path'),
    prettyMS = require('pretty-ms'),
    spawn = require('child_process').spawn,
    pos = require("cli-position"),
    validate = require('aproba'),
    got = require('got'),
    cheerio = require('cheerio'),
    Gauge = require("gauge"),
    EventEmitter = require('events'),
    colors = require('colors'),
    numeral = require('numeral'),
    ee = new EventEmitter();


let cmdWidth = pos.columns() - 5,
    files, OPTIONS,
    playerStatus = 'stopped',
    totalFiles,
    playingItemNumber = 0,
    pauseDuration = 0,
    timeoutInterval;


const themes = require('gauge/themes'),

    // create a new theme based on the color ascii theme for this platform
    // that brackets the progress bar with arrows
    ourTheme = themes.newTheme(themes({ hasUnicode: false, hasColor: true }), {
        progressbarTheme: {
            complete: '>',
            remaining: ' '
        },
        activityIndicatorTheme: '⠋⠙⠹⠸⠼⠴⠦⠧⠇⠏',
        preSubsection: '>',
        preProgressbar: '[',
        postProgressbar: ']',
    }),
    gauge = new Gauge({ theme: ourTheme });




function play_files(videos, options) {

    validate("AO", arguments)

    OPTIONS = options;

    // process.stdin.resume(); //so the program will not close instantly

    // function exitHandler(options, exitCode) {

    //     if (options.cleanup) clear_console();
    //     if (exitCode || exitCode === 0)
    //         if (options.exit) process.exit();
    // }

    // //do something when app is closing
    // process.on('exit', exitHandler.bind(null, { cleanup: true }));

    // //catches ctrl+c event
    // process.on('SIGINT', exitHandler.bind(null, { exit: true }));

    // // catches "kill pid" (for example: nodemon restart)
    // process.on('SIGUSR1', exitHandler.bind(null, { exit: true }));
    // process.on('SIGUSR2', exitHandler.bind(null, { exit: true }));

    // //catches uncaught exceptions
    // process.on('uncaughtException', exitHandler.bind(null, { exit: true }));


    return new Promise((resolve, reject) => {

        files = videos.filter(o => o).map(o => {
            o.duration = typeof o.duration == 'number' ?
                o.duration * 1000 :
                duration_to_seconds(o.duration || '0') * 1000;
            return o
        });

        let totalPlaytime = files.map(o => o.duration).reduce((a, b) => a + b, 0);

        totalFiles = files.length;

        console.log();
        console.log(('-'.repeat(20) + ' YT-VLC Mp3 Player ' + '-'.repeat(20)).bold.red);
        console.log(`>> ${totalFiles } mp3 files added to playlist. Total Playtime is ${prettyMS(totalPlaytime)}. \n`);



        ee.on('play-file', function(text) {

            if (totalFiles) {
                // pick file
                let file = files.shift(),
                    nextFile = files[0] ? files[0] : null;

                play(file, nextFile);

            } else {
                // clear_console();
                console.log(">> Playback Finished");
                console.log("");
                resolve();
            }

        })

        ee.on('load-next-mp3', function() {
            // console.log('Load next file');
            // loads file then updates the file url with the right mp3

            if (files && files.length) {
                let file = files[0];
                playingItemNumber++;


                console.log('-'.repeat(pos.columns()).gray);
                console.log(`>> Track [${playingItemNumber}/${totalFiles}]: ${file.title}`);

                get_Mp3(file.id)
                    .then((mp3Links) => {
                        //TODO: get file depending on bitrate expected
                        url = mp3Links[0];
                        return url;
                    })
                    .then((url) => {
                        files[0].url = url;
                        // console.log(url);
                        ee.emit('play-file');

                    })
                    .catch(console.error);

            }


        });


        ee.emit('load-next-mp3');
        // clear
        // player_title();

    })

}




async function get_Mp3(youtubeID) {

    validate("S", arguments)
    let baseLink = 'https://www.yt-download.org/api/button/mp3/',
        URL = baseLink + youtubeID;

    return got(URL)
        .then((response) => {
            return parse_html(response.body)
        })
}

async function parse_html(html) {

    let $ = cheerio.load(html);

    let a = $('.download  a').map(function() {
        return $(this).attr('href')
    }).get()

    return a

}

function duration_to_seconds(str) {;

    let seconds = [1, 60, 3600],
        arr = str.split(':').reverse(),
        duration = arr.map((d, i) => Number(d) * seconds[i])
        .reduce((a, b) => a + b, 0);

    return duration;


}

function play(file, nextFile) {

    let args = [
        '-vn',
        '-hide_banner',
        '-stats',
        '-nodisp',
        '-volume', OPTIONS.mp3Volume,
        '-infbuf',
        '-stats',
        '-autoexit',

        file.url
    ]

    let child = spawn('ffplay', args);

    // reset pause duration
    // pauseDuration = 0;

    // set this interval only once
    if (!timeoutInterval) {

        // start documenting pause
        timeoutInterval = setInterval(() => {

            // kill child if we have paused for more than timeout
            if (pauseDuration > OPTIONS.mp3Timeout) {
                console.log(`>> Skipping ${file.title}...`);
                pauseDuration = 0;
                child.kill()
            }

            // increment pause duration
            pauseDuration++;
        }, 1000);

    }



    // console.log(file.url);

    child.stdout.on('data', (data) => {
        // console.log(`child stdout:\n${data}`);
    });

    let str, time, lastTime;

    child.stderr.on('data', (data) => {

        // reset pause
        pauseDuration = 0;

        str = data.toString().trim();
        time = str.split(" ").shift();

        playerStatus = 'playing';

        if (/[0-9]+/.test(time)) {

            // if time has changed and is a modulus of 7 (so as to throttle)
            if (lastTime !== time && (time * 1000) % 7 == 0) {

                out(file, time, child);

                lastTime = time;
            }
        }

    });

    child.on('exit', function(code, signal) {
        // console.log('child process exited with ' +  `code ${code} and signal ${signal}`);
        ee.emit('load-next-mp3');
    });

}



function out(file, time, child) {

    time = parseFloat(time) || 0;
    let duration = parseFloat(file.duration) || 0,
        percentage = (time * 1000) / duration,
        timeString = prettyMS(time * 1000).split(" ")
        .map(s => {
            return s.replace(/([0-9]+)([a-z]+)/i, (a, b, c) => {
                return numeral(b).format('00.0') + c
            })
        }).join(' '),
        metaStr = `${timeString}/${prettyMS(duration)} (${numeral(percentage*100).format('00.0')}%)`;


    gauge.pulse();
    gauge.show(`${metaStr.padEnd(30, ' ')} ${('[>]').bold.red} ${(file.title).italic.gray}`, percentage);

}


// function clear_console() {
//     // process.stdout.write('\x1Bc');
// }



function text_right(str, row = 0) {
    pos.moveTo(cmdWidth - stripAnsi(str).length + 2, row)
    console.log(str);
}


function stripAnsi(string) {
    if (typeof string !== 'string') {
        throw new TypeError(`Expected a \`string\`, got \`${typeof string}\``);
    }

    return string.replace(ansiRegex(), '');
}

function ansiRegex({ onlyFirst = false } = {}) {
    const pattern = [
        '[\\u001B\\u009B][[\\]()#;?]*(?:(?:(?:[a-zA-Z\\d]*(?:;[-a-zA-Z\\d\\/#&.:=?%@~_]*)*)?\\u0007)',
        '(?:(?:\\d{1,4}(?:;\\d{0,4})*)?[\\dA-PR-TZcf-ntqry=><~]))'
    ].join('|');

    return new RegExp(pattern, onlyFirst ? undefined : 'g');
}



/**/






module.exports = play_files;