

# Run VLC & Play YouTube from CLI.
I developed this module because I like to have a background video playing behind my translucent CLI (cmder). 

## How it works
YT-VLC fetches video links from YouTube and sends them over to VLC. It also saves the same as an m3u playlist so you can always stream the same videos using any other player that streams YouTube such as [PotPlayer](https://potplayer.daum.net/).

## Prerequisites
- You will need to install [NodeJs](https://nodejs.org/en/)
- You will also need [VLC](https://www.videolan.org/vlc/)
- Ensure your VLC can stream YouTube and if not, then follow the instructions [HERE](https://forum.videolan.org/viewtopic.php?t=156829#p515642)

- You will also need to enable VLC's Web interface as demonstrated [HERE](https://github.com/azrafe7/vlc4youtube/blob/master/instructions/how-to-enable-vlc-web-interface.md)

  Take note of the password and username you set up as you will need it. Note that username is optional for VLC Web interface.

- This module can also stream the files as mp3 files via command prompt...



## Installing 
Run ```npm install --global yt-vlc```

**NOTE:** Not sure why ```yarn global insall``` fails. Any ideas? 

After installation, then the magic starts. Try and run ```yt-vlc --help```;

![--help image](https://bitbucket.org/mugendi/yt-vlc/raw/a26dfc2ea32f98986ac1f6ef2e2187a02c629075/assets/1.png)

### Setting Up Your Configurations
It is easier to just save your VLC Web Interface logins once so you wont need to set them again.

To do this you simply type the following commands.

```yt-vlc --set_password "your-vlc-password"``` to set you password config.

```yt-vlc --set_username "this_is_me"``` to set you username config. 

Now you are ready to start playing!

### Streaming
First ensure VLC is running. If you just configured VLC Web Interface, then restart it for the new configurations to take effect.

Below are the commands you need.

1.  ```yt-vlc eminem``` or ```yt-vlc -s eminem ``` Search, save playlist, play eminem videos
2.  ```yt-vlc -s eminem --no-play``` Search, save playlist but don't start playing eminem videos.
3.  ```yt-vlc -s eminem --no-save``` Search and play eminem videos. But don't save the playlist files.

**NOTE:** Always quote multiple word phrases such as ```yt-vlc "eminem latest songs"```

**Need to play entire playlists?** then here is the magic.

4.  ```yt-vlc -l "https://www.youtube.com/watch?v=JGwWNGJdvx8&list=PLhsz9CILh357zA1yMT-K5T9ZTNEU6Fl6n"``` fetches all the videos in playlist, saves playlist file and starts playing.   
  You can also apply other flags above to stop playlist saving or immediate playback.

 **NOTE:** Playlists take **loooong** to process so avoid for now. Will fix this in future release.


### Extra commands

You can remove a config value by calling the set command with a blank value. For example, to remove your password, run :
```yt-vlc --set_password ```

You can also set the **"playlist name"** by using the ```-n, --name``` flag.

Otherwise use ```--help``` to have a look at all available options.

## Extra Resource
Read [This](https://www.vlchelp.com/stop-hd-video-from-freezing-vlc/) to optimize your VLC streaming experience.
