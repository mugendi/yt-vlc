// https: //kworb.net/youtube/

const got = require('got'),
    cheerio = require('cheerio'),
    validate = require('aproba');

let baseLink = 'https://www.yt-download.org/api/button/mp3/QEfWfV438H4';


// 

// get_Mp3("ek_kj2t4vko");

async function get_Mp3(youtubeID) {

    validate("S", arguments)

    let URL = baseLink + youtubeID;

    got(URL)
        .then((response) => {
            return parse_html(response.body)
        })
        .then((resp) => {
            console.log(resp)
        })
        .catch(console.error)
}

async function parse_html(html) {

    let $ = cheerio.load(html);

    let a = $('.download  a').map(function() {
        return $(this).attr('href')
    }).get()

    return a

}



const playlistID = 'PLgK2kionQ-hmU4FWLrAqiyPYjWiR8zuWl'
    // PLgK2kionQ-hmU4FWLrAqiyPYjWiR8zuWl

var ytpl = require('ytpl');

get_playlist(playlistID);

async function get_playlist(playlistID) {
    const playlist = await ytpl(playlistID);

    console.log(playlist);
}