#!/usr/bin/env node


const meow = require('meow'),
    Conf = require('conf');


const cli = meow(`

	Options
	  --search, -q      Query to search YouTube
      --pages           The number of pages to return. Each page contains about 20 results. Default is 1 
      --playlist, -l    Playlist to fetch. Enter either link or playlist id.
      --playlist_limit  Number of items retrieved from a playlist. Default, Infinity
      --cache_duration  Duration that your playlists & search results are cached. Defaults to 72 (hours)

      --dir, d          Directory to save playlist. Defaults to "Desktop/vlc-playlists"
      --name, -n        Playlist Name. Defaults to playlist ID or search Query
      --save, -s        Save Playlist. Default is true      
      --play, p         Start Playing in VLC. Default is true  
      --shuffle         Shuffle videos pay order from that of search results or playlist. 

      --set_password    Set and persist VLC Web Interface Login password
      --set_username    Set and persist VLC Web Interface Login username
      --server          VLC Web Interface server/IP
      --port            VLC Web Interface port
      --username        VLC Web Interface Login username
      --password        VLC Web Interface Login password

      --as_mp3          Immediately start playing in command line as MP3's
      --mp3_volume      Set MP3 Playback volume. Default is 75. Expected values are 0-100.
      --mp3_timeout     Duration after which loading a file fails and player tries to play the next file. Default is 20 (seconds)

	  $ yt-vlc --search eminem --no-play
	  
`, {
    flags: {
        search: {
            type: 'string',
            alias: 'q'
        },
        playlist: {
            type: 'string',
            alias: 'l'
        },
        save: {
            type: "boolean",
            alias: 's',
            default: true
        },
        play: {
            type: "boolean",
            alias: "p",
            default: true
        },
        dir: {
            type: "string",
            alias: "d"
        },
        name: {
            type: "string",
            alias: "n"
        },
        shuffle: {
            type: "boolean",
            default: false
        },
        playlist_limit: {
            type: "number",
            default: Infinity
        },
        port: {
            type: "number",
            default: 8080
        },
        server: {
            type: "string",
            default: "localhost"
        },
        password: {
            type: "string"
        },
        username: {
            type: "string"
        },
        set_password: {
            type: "string"
        },
        set_username: {
            type: "string"
        },

        as_mp3: {
            type: 'boolean'
        },
        mp3_volume: {
            type: "number",
            default: 75
        },
        mp3_timeout: {
            type: "number",
            default: 20
        },

        pages: {
            type: "number",
            default: 1
        },

        cache_duration: {
            type: "number",
            default: 24 * 3
        }

    }
});



// Create a Configstore instance.
const config = new Conf();


let options = Object.assign({ search: cli.input[0] }, cli.flags);



function setConfig(obj) {

    // set/remove config values
    for (let key in obj) {
        // if value
        if (obj[key].length) {
            // add
            // configs[key] = obj[key];
            config.set(`settings.${key}`, obj[key])
            console.log(`>> Successfully set ${key}`);
        } else {
            // No value means we delete this option
            config.delete(`settings.${key}`);
        }
    }

}

// set password & username
if (options.hasOwnProperty('setPassword')) setConfig({ password: options.setPassword })
if (options.hasOwnProperty('setUsername')) setConfig({ username: options.setUsername })


options = Object.assign({}, config.get('settings') || {}, options)

if (options.search || options.playlist) {
    require("..")(options);
}