const ytsr = require('ytsr'),
    ytpl = require('ytpl'),
    fs = require('fs-extra'),
    path = require("path"),
    validate = require('aproba'),
    ospath = require('ospath'),
    qs = require('qs'),
    VLC = require("vlc-client"),
    filenamify = require('filenamify'),
    open = require('open'),
    vlcCommand = require('vlc-command'),
    ora = require('ora'),
    validUrl = require('valid-url'),
    prettyMS = require('pretty-ms'),
    envPaths = require('env-paths'),
    m3uWriter = require('m3u').extendedWriter();




const defaultPlaylistsDir = path.join(ospath.desktop(), 'vlc-playlists'),
    player = require('./lib/mp3-player'),
    appEnvPaths = envPaths('yt-vlc');


// some globals
let vlc, playlistName, OPTIONS, spinner;

module.exports = async(opts) => {
    // set some globals
    OPTIONS = opts;

    spinner = ora('>> Busy...').start();

    // if we need VLC
    if (OPTIONS.play && !OPTIONS.asMp3) {
        // setup vlc 
        await ensure_vlc(opts);
    }

    // playlist takes precedence to search
    if (opts.playlist) {
        let args = [opts.playlist, opts.name, opts.dir].filter(v => v);
        playlist(...args)
    } else if (opts.search) {
        let args = [opts.search, opts.name, opts.dir].filter(v => v);
        search(...args)
    }

}


function log(text) {
    spinner.info(text).start()
}





async function playlist(playlistID, name = null, dir) {

    validate("SSS|SZS|SSZ|SZZ|SS|S", arguments);

    dir = dir || defaultPlaylistsDir;

    // format proper playlist id
    if (/\?/.test(playlistID)) {
        let o = qs.parse(playlistID);

        if (o.list) {
            playlistID = o.list;
        } else {
            throw "Playlist ID could not be retrieved from: " + playlistID
        }

    }


    let playlistDir = playlist_dir(dir),
        playlistFile;


    log(`>> Fetching playlist for: ${playlistID}`);

    // console.log(playlist);
    // check if file is not cached

    let cachedData = cached_file(playlistID, 'playlist'),
        cachedFilePath = get_cache_file_path(playlistID, 'playlist');

    if (cachedData) {
        log(`>> Loaded cached playlist... Cache expires in ${OPTIONS.cacheDuration} hrs`);
        return do_what_with_result(cachedData.videos, cachedData.playlistFile)
    }


    const playlist = await ytpl(playlistID, { limit: Infinity });

    playlistName = filenamify(name || playlist.title);

    playlistFile = path.join(playlistDir, "video-playlist-" + playlistName + '.m3u');

    // format videos...
    let videos = playlist.items.map(o => {
        o.duration = o.durationSec;
        return o;
    });


    fs.writeJsonSync(cachedFilePath, { playlistFile, videos });


    return do_what_with_result(videos, playlistFile);

}



async function search(query, name = null, dir) {

    validate("SSS|SZS|SSZ|SZZ|SS|S", arguments);

    dir = dir || defaultPlaylistsDir;

    // make playlist name & save
    let playlistDir = playlist_dir(dir),
        playlistFile;

    playlistName = filenamify(name || query);

    playlistFile = path.join(playlistDir, playlistName + '.m3u');

    let cachedData = cached_file(playlistName, 'search'),
        cachedFilePath = get_cache_file_path(playlistName, 'search');

    if (cachedData) {
        log(`>> Loaded cached results... Cache expires in ${OPTIONS.cacheDuration} hrs`)
        return do_what_with_result(cachedData, playlistFile)
    }

    log(`>> Searching for ${query}`);

    const search = await ytsr(query, { pages: OPTIONS.pages }),
        videos = search.items.filter(o => o.type == 'video');

    // save file
    // console.log(cachedFilePath);
    fs.writeJsonSync(cachedFilePath, videos);

    return do_what_with_result(videos, playlistFile)

}



// ensures vlc is running
async function ensure_vlc(opts) {


    vlc = new VLC.Client({
        ip: opts.server,
        port: opts.port,
        username: opts.username, //username is optional
        password: opts.password
    })


    let status = await vlc.status().catch(err => err.code);


    if (status == 'ECONNREFUSED') {
        log(`>> It seems like VLC is not running.... Attempting to open...`);
    } else {
        // all good return 
        return;
    }

    let vlcPath = await new Promise((resolve, reject) => {
        vlcCommand(function(err, vlcPath) {
            if (err) reject(err);
            else resolve(vlcPath)
        })
    }).catch(err => null);

    if (!vlcPath) {
        spinner.fail('>> Could not find VLC path!');
        log('Try Open VLC manually');
        return;
    }

    // attempt to open
    let vlcPID = await open(vlcPath).then(resp => resp.pid).catch(err => null);

    if (vlcPID) {

        return new Promise((resolve, reject) => {
            // wait a little to be sure that VLC is ready to receive commands
            setTimeout(() => {
                log('>> VLC Open');
                resolve()
            }, 1000);
        });

    } else {
        spinner.fail('>> Could not open VLC!');
        log('Try Open VLC manually');
        return
    }

}



function get_cache_file_path(name, type) {
    validate("SS", arguments);

    fs.ensureDirSync(appEnvPaths.data);

    let fileName = filenamify(`${type}-${name}.json`),
        filePath = path.join(appEnvPaths.data, fileName);

    return filePath;
}

function cached_file(name, type) {
    validate("SS", arguments);

    let filePath = get_cache_file_path(name, type);

    if (fs.existsSync(filePath)) {
        // get stats
        let stats = fs.statSync(filePath),
            now = Date.now(),
            cacheDuration = OPTIONS.cacheDuration * (3600 * 1000);

        // if cache is still valid
        if (stats.ctimeMs + cacheDuration > now) {
            return fs.readJsonSync(filePath);
        }

    }

    return null;
}


function shuffleArray(array) {
    for (let i = array.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        const temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }
}


function do_what_with_result(videos, playlistFile) {

    validate("AS", arguments);


    if (OPTIONS.shuffle) {
        shuffleArray(videos)
    }


    if (OPTIONS.asMp3) {
        spinner.clear()
        spinner.stop();
        return player(videos, OPTIONS);
    }


    // console.log(resp)
    return new Promise((resolve, reject) => {

            // save playlist
            if (OPTIONS.save) {
                log(`>> Saving video playlist to: ${playlistFile}`);
                // do we want to save response in playlist file?
                to_playlist(videos, playlistFile)
                    .then(resolve)
                    .catch(reject)
            } else resolve()


        })
        .then((resp) => {
            // what to do with vlc?
            return new Promise((resolve, reject) => {

                // do we want to play ?
                if (OPTIONS.play) {
                    log(`>> Sending playlist to VLC`);
                    return vlc.emptyPlaylist()
                        .then((resp) => {
                            return to_vlc(videos)
                        })
                        .then((resp) => {
                            log(`>> Asking VLC to start playing....`);
                            // start playing
                            return vlc.playFromPlaylist(0);
                        })
                        .then(resolve)
                } else resolve()

            });
        })
        .then((resp) => {
            spinner.succeed("Done!")
        })
        .catch(console.error)




}






function playlist_dir(dir) {
    validate("S", arguments);

    let playlistDir;

    if (path.isAbsolute(dir)) {
        playlistDir = path.normalize(dir);
    } else {
        playlistDir = path.join(__dirname, dir);
    }

    //ensure dir
    fs.ensureDirSync(playlistDir);


    return playlistDir;

}

async function to_vlc(videos) {
    validate("A", arguments);

    let link;

    for (let video of videos) {
        if (video) {
            link = 'https://www.youtube.com/watch?v=' + video.id;
            // only send correct links...
            if (validUrl.isUri(link)) {
                await vlc.addToPlaylist(link)
            }
        }
    }


}


function to_playlist(videos, playlistFile) {

    validate('AS', arguments)

    return make_playlist(videos)
        .then((playlist_content) => {
            fs.writeFileSync(playlistFile, playlist_content);
            return playlistFile;
        })
        .catch(console.error)

}

function duration_to_seconds(str) {

    let seconds = [1, 60, 3600],
        arr = str.split(':').reverse(),
        duration = arr.map((d, i) => Number(d) * seconds[i])
        .reduce((a, b) => a + b, 0);

    return duration;


}


function video_to_m3u(video, num, url) {

    url = url || 'https://www.youtube.com/watch?v=' + video.id;

    let duration;

    if (video) {
        // Video Object
        video = Object.assign({ title: "Unknown Title", artist: "Unknown Artist", duration: 0 }, video);

        duration = typeof video.duration == 'number' ? video.duration : duration_to_seconds(video.duration || '0');
        // Add comment
        m3uWriter.comment(`[${num}] - ${video.title} by ${video.artist|| "*"} - ${prettyMS(duration * 1000)}`);
        // A playlist item, usually a path or url.
        m3uWriter.file(url, duration, video.title);
        // An empty line.
        m3uWriter.write();
    }

}

async function make_playlist(videos) {

    let duration, num = 0;

    for (let video of videos) {

        num++;

        // if there is a video 
        video_to_m3u(video, num, video.url);

    }

    let output = m3uWriter.toString();

    return output;

}